# Bayu News

News apps from all the world. This project using VIPER, Modularization, SPM and Unit Test.

# Architecture

I use VIPER for this project. For all detail related with architecture that I used you can check [here](https://github.com/infinum/iOS-VIPER-Xcode-Templates).

## Library
* [Kingfisher](https://github.com/onevcat/Kingfisher)

## Project Structure

<Img src="screenshoots/Screenshot 2023-07-19 at 04.14.40.png" width="200">

* BNNetworkService: Module for handling all things about network service.
* BNUIKit: Everything about UIKit code such as UIKit extension, Custom View,etc is here.
* CommonUI: Module for common UIViewController like web page.
* NewsCore: Module related with model and services related with news.
* NewsUI: All features related with News UI and implementation, save here.
* Test: All test here.

Note: 
* UI Module can't import another UI module. Depedency cycle error can be happened.
* A UI Module can have more than a Core Module.
* View controller navigation between module using factory.

## Screenshoot
<Img src="screenshoots/Simulator Screenshot - iPhone 14 Pro - 2023-07-19 at 03.23.55.png" width="200">
![demo 1](screenshoots/Simulator Screen Recording - iPhone 14 Pro - 2023-07-19 at 03.29.04.mp4)
![demo 2](screenshoots/Simulator Screen Recording - iPhone 14 Pro - 2023-07-19 at 03.29.15.mp4)
![demo 3](screenshoots/Simulator Screen Recording - iPhone 14 Pro - 2023-07-19 at 03.29.38.mp4)