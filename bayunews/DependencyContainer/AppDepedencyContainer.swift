import Environment
import UIKit
import NewsUI
import CommonUI

protocol AppDepedencyContainerType {
    func makeCategoriesViewController() -> UIViewController
    func makeSourcesViewController(category: String) -> UIViewController
    func makeHeadlinesViewController(category: String, source: String) -> UIViewController
    func makeWebPageViewController(title: String, url: String) -> UIViewController
}

final class AppDepedencyContainer: AppDepedencyContainerType {
    func makeCategoriesViewController() -> UIViewController {
        return CategoriesWireframe(factory: self).viewController
    }
    
    func makeSourcesViewController(category: String) -> UIViewController {
        return SourcesWireframe(factory: self, category: category).viewController
    }
    
    func makeHeadlinesViewController(category: String, source: String) -> UIViewController {
        return HeadlinesWireframe(
            factory: self,
            category: category,
            source: source
        ).viewController
    }

    func makeWebPageViewController(title: String, url: String) -> UIViewController {
        return WebPageViewController(title: title, url: url)
    }
}

extension AppDepedencyContainer: CategoriesViewControllerFactory {}
extension AppDepedencyContainer: HeadlinesViewControllerFactory {}
extension AppDepedencyContainer: SourcesViewControllerFactory {}
