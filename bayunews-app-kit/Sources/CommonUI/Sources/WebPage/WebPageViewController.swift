import UIKit
import BNUIKit
import WebKit

public final class WebPageViewController: UIViewController {

    private lazy var webView: WKWebView = {
        let view = WKWebView()
        view.backgroundColor = .clear
        view.isOpaque = false
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let titleNews: String
    private let url: String
    
    public init(title: String, url: String) {
        self.titleNews = title
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        title = titleNews
        view.addSubview(webView)
        webView.anchorAll(to: view)
        guard let url = URL(string: url) else { return }
        webView.load(.init(url: url))
    }
}
