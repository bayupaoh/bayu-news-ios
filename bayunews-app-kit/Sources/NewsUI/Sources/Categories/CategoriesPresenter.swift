import Foundation

protocol CategoriesPresenterInterface {
    func viewDidLoad()
    func navigateToSourcePage(category: String)
}

final class CategoriesPresenter: CategoriesPresenterInterface {
    private unowned let view: CategoriesViewInterface
    private let interactor: CategoriesInteractorInterface
    private let wireframe: CategoriesWireframeInterface
    
    init(
        view: CategoriesViewInterface,
        interactor: CategoriesInteractorInterface,
        wireframe: CategoriesWireframeInterface
    ) {
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
    }

    func viewDidLoad() {
        let categories = interactor.fetchCategories()
        view.setCategories(categories: categories)
    }
    
    func navigateToSourcePage(category: String) {
        wireframe.navigateToSourcesPage(category: category)
    }
}
