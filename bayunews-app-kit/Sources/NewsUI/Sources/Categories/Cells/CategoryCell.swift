import UIKit
import BNUIKit

final class CategoryCell: UITableViewCell {

    private let titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .black
        view.textAlignment = .left
        view.font = .boldSystemFont(ofSize: 18.0)
        view.numberOfLines = 0
        return view
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        configureTitle()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unimplemented")
    }
    
    public func configure(title: String) {
        titleLabel.text = title
    }
}

private extension CategoryCell {
    func configureTitle() {
        contentView.addSubview(titleLabel)
        titleLabel.anchorAll(to: contentView, constant: .init(16.0, 4.0, 16.0, 4.0))
    }
}
