import Foundation
import BNUIKit
import NewsCore
import UIKit

protocol CategoriesWireframeInterface {
    func navigateToSourcesPage(category: String)
}

public protocol CategoriesViewControllerFactory {
    func makeSourcesViewController(category: String) -> UIViewController
}

public class CategoriesWireframe: BaseWireframe {
    let factory: CategoriesViewControllerFactory
    
    public init(factory: CategoriesViewControllerFactory) {
        self.factory = factory
        let moduleViewController = CategoriesViewController()
        super.init(viewController: moduleViewController)

        let interactor = CategoriesInteractor(service: NewsService())
        let presenter = CategoriesPresenter(
            view: moduleViewController,
            interactor: interactor,
            wireframe: self
        )
        moduleViewController.presenter = presenter
    }
}

extension CategoriesWireframe: CategoriesWireframeInterface {
    func navigateToSourcesPage(category: String) {
        let destination = factory.makeSourcesViewController(category: category)
        navigationController?.pushViewController(destination, animated: true)
    }
}
