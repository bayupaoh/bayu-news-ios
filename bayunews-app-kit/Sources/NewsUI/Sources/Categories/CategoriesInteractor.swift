import Foundation
import NewsCore

public protocol CategoriesInteractorInterface {
    func fetchCategories() -> [CategoryItem]
}

final public class CategoriesInteractor: CategoriesInteractorInterface {
    
    let service: NewsServicesInterfaces
    
    init(service: NewsServicesInterfaces) {
        self.service = service
    }
    
    public func fetchCategories() -> [CategoryItem] {
        return service.fetchCategories()
    }
}
