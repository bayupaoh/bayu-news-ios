import UIKit
import NewsCore

protocol CategoriesViewInterface: AnyObject {
    func setCategories(categories: [CategoryItem])
}

public final class CategoriesViewController: UITableViewController {

    var presenter: CategoriesPresenterInterface!
    
    private var categories: [CategoryItem] = []
    
    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.title = "Select Category"
        configureTableView()
        presenter.viewDidLoad()
    }
    
    public override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    public override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: CategoryCell.self),
            for: indexPath
        ) as? CategoryCell else { fatalError() }
        
        let item = categories[indexPath.row].title ?? ""
        cell.configure(title: item)
        return cell
    }
    
    public override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let category = categories[indexPath.row].value else { return }
        presenter.navigateToSourcePage(category: category)
    }
}

extension CategoriesViewController: CategoriesViewInterface {
    func setCategories(categories: [CategoryItem]) {
        self.categories = categories
        tableView.reloadData()
    }
}

private extension CategoriesViewController {
    func configureTableView() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 40.0
        tableView.separatorStyle = .singleLine
        tableView.register(CategoryCell.self, forCellReuseIdentifier: String(describing: CategoryCell.self))
    }
}
