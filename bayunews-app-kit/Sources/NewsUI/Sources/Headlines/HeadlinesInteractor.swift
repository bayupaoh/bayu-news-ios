import Foundation
import NewsCore

public protocol HeadlinesInteractorInterface {
    var page: Int { get set }
    var isRequestInFlight: Bool { get set }
    func fetchHeadlines(
        keyword: String?,
        completion: @escaping (Result<[Article], Error>) -> Void
    )
}

final public class HeadlinesInteractor: HeadlinesInteractorInterface {
    
    let service: NewsServicesInterfaces
    let category: String
    let source: String
    
    public var page: Int = 1
    public var isRequestInFlight: Bool = false
    
    init(category: String, source: String, service: NewsServicesInterfaces) {
        self.service = service
        self.source = source
        self.category = category
    }
    
    public func fetchHeadlines(
        keyword: String?,
        completion: @escaping (Result<[Article], Error>) -> Void
    ) {
        guard (keyword ?? "").count >= 3 || (keyword ?? "").isEmpty else { return }
        isRequestInFlight = true
        service.fetchHeadlines(
            source: source,
            category: category,
            keyword: keyword,
            page: page) { [weak self] result in
                self?.isRequestInFlight = false
                switch result {
                case .success(let response):
                    self?.page += 1
                    completion(.success(response.articles ?? []))
                case .failure(let error):
                    completion(.failure(error))
                }
            }
    }
}
