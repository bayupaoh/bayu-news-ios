import Foundation
import NewsCore

protocol HeadlinesPresenterInterface {
    func fetchHeadlines(keyword: String?, isFirstLoad: Bool)
    func navigateToWebPage(row: Int)
    func headleLoadMore(row: Int)
}

final class HeadlinesPresenter: HeadlinesPresenterInterface {
    private unowned let view: HeadlinesViewInterface
    private var interactor: HeadlinesInteractorInterface
    private let wireframe: HeadlinesWireframeInterface
    private let formatter: HeadlinesFormatterInterface
    
    private var articles: [Article] = []
    private var keyword: String?
    
    init(
        view: HeadlinesViewInterface,
        interactor: HeadlinesInteractorInterface,
        wireframe: HeadlinesWireframeInterface,
        formatter: HeadlinesFormatterInterface
    ) {
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
        self.formatter = formatter
    }
    
    func fetchHeadlines(keyword: String?, isFirstLoad: Bool) {
        if isFirstLoad {
            view.showLoading()
            interactor.page = 1
        }
        self.keyword = keyword
        interactor.fetchHeadlines(keyword: keyword) { [weak self] result in
            guard let self else { return }
            switch result {
            case .success(let articles):
                if isFirstLoad {
                    view.hideLoading()
                    self.articles = articles
                } else {
                    self.articles.append(contentsOf: articles)
                }
                let cells = self.formatter.makeHeadlineCellModel(self.articles)
                self.view.showHeadlines(cells: cells)
            case .failure:
                if isFirstLoad {
                    self.view.showErrorView()
                } else {
                    self.view.showMessage("There is something wrong.. Please try again")
                }
            }
        }
    }
    
    func navigateToWebPage(row: Int) {
        guard let url = articles[row].url else { return }
        wireframe.navigateToWebViewPage(
            title: articles[row].title ?? "",
            url: url
        )
    }
    
    func headleLoadMore(row: Int) {
        let lastIndex = articles.count - 1
        guard row >= lastIndex,
              !interactor.isRequestInFlight,
              interactor.page != 1
        else { return }
        fetchHeadlines(keyword: keyword, isFirstLoad: false)
    }
}
