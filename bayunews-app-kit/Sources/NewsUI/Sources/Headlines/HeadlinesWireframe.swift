import Foundation
import BNUIKit
import NewsCore
import UIKit

protocol HeadlinesWireframeInterface {
    func navigateToWebViewPage(title: String, url: String)
}

public protocol HeadlinesViewControllerFactory {
    func makeWebPageViewController(title: String, url: String) -> UIViewController
}

public class HeadlinesWireframe: BaseWireframe {
    private let factory: HeadlinesViewControllerFactory
    
    public init(
        factory: HeadlinesViewControllerFactory,
        category: String,
        source: String
    ) {
        self.factory = factory
        
        let moduleViewController = HeadlinesViewController()
        super.init(viewController: moduleViewController)

        let interactor = HeadlinesInteractor(
            category: category,
            source: source,
            service: NewsService()
        )

        let formatter = HeadlinesFormatter()

        let presenter = HeadlinesPresenter(
            view: moduleViewController,
            interactor: interactor,
            wireframe: self,
            formatter: formatter
        )
        moduleViewController.presenter = presenter
    }
}

extension HeadlinesWireframe: HeadlinesWireframeInterface {
    func navigateToWebViewPage(title: String, url: String) {
        let destination = factory.makeWebPageViewController(title: title, url: url)
        navigationController?.pushViewController(destination, animated: true)
    }
}
