import UIKit
import BNUIKit

struct HeadlineCellModel {
    let title: String?
    let desc: String?
    let imageURL: String?
}

final class HeadlineCell: UITableViewCell {
    private let titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .black
        view.textAlignment = .left
        view.font = .boldSystemFont(ofSize: 14.0)
        view.numberOfLines = 1
        return view
    }()
    
    private let descLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .black
        view.textAlignment = .left
        view.font = .systemFont(ofSize: 12.0)
        view.numberOfLines = 3
        return view
    }()

    private let thumbnail: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleToFill
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unimplemented")
    }
    
    func configure(_ cellModel: HeadlineCellModel) {
        titleLabel.text = cellModel.title
        descLabel.text = cellModel.desc
        thumbnail.loadImage(url: cellModel.imageURL)
    }
}

extension HeadlineCell {
    func setupView() {
        contentView.layer.cornerRadius = 8.0
        
        contentView.addSubview(thumbnail)
        thumbnail.anchorLeadingTrailing(
            toLeading: contentView.leadingAnchor,
            toTrailing: contentView.trailingAnchor
        )
        thumbnail.anchorHeightSize(constant: 100.0, type: .constant)
        thumbnail.anchorTop(to: contentView.topAnchor)

        contentView.addSubview(titleLabel)
        titleLabel.anchorLeadingTrailing(
            toLeading: contentView.leadingAnchor,
            toTrailing: contentView.trailingAnchor,
            constant: .init(16.0, 16.0)
        )
        titleLabel.anchorTop(to: thumbnail.bottomAnchor, constant: 8.0)

        contentView.addSubview(descLabel)
        descLabel.anchorLeadingTrailing(
            toLeading: contentView.leadingAnchor,
            toTrailing: contentView.trailingAnchor,
            constant: .init(16.0, 16.0)
        )
        descLabel.anchorTopBottom(
            toTop: titleLabel.bottomAnchor,
            toBottom: contentView.bottomAnchor,
            constant: .init(8.0, 8.0)
        )
    }
}
