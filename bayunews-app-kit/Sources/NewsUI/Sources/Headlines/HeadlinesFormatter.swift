import Foundation
import NewsCore

protocol HeadlinesFormatterInterface {
    func makeHeadlineCellModel(_ articles: [Article]) -> [HeadlineCellModel]
}

public class HeadlinesFormatter: HeadlinesFormatterInterface {
    init() { }
    
    func makeHeadlineCellModel(_ articles: [Article]) -> [HeadlineCellModel] {
        let cells = articles.map { HeadlineCellModel(
            title: $0.title,
            desc: $0.description,
            imageURL: $0.urlToImage
        ) }
        return cells
    }
}
