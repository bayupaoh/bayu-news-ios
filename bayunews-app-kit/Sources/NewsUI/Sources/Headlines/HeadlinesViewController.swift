import UIKit
import BNUIKit

protocol HeadlinesViewInterface: AnyObject {
    func showLoading()
    func hideLoading()
    func showHeadlines(cells: [HeadlineCellModel])
    func showErrorView()
    func showMessage(_ message: String)
}

final class HeadlinesViewController: UIViewController, ErrorableViewController {
    var errorViewController: ErrorViewController?
    
    private let tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.rowHeight = UITableView.automaticDimension
        view.estimatedRowHeight = 80.0
        view.separatorStyle = .singleLine
        view.keyboardDismissMode = .onDrag
        return view
    }()

    private let refreshControl: UIRefreshControl = {
        let view = UIRefreshControl()
        view.backgroundColor = .white
        view.tintColor = .systemBlue
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let searchBar: UISearchBar = {
        let view = UISearchBar()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.placeholder = "Find article"
        view.tintColor = .black
        return view
    }()

    private var cells: [HeadlineCellModel] = []
    private var keyword: String?
    
    var presenter: HeadlinesPresenterInterface!

    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        setupTableView()
        setupRefreshCotnrol()
        setupSearchBar()
        presenter.fetchHeadlines(keyword: keyword, isFirstLoad: true)
    }
}

private extension HeadlinesViewController {
    func setupTableView() {
        view.addSubview(tableView)
        tableView.anchorAll(to: view)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(HeadlineCell.self, forCellReuseIdentifier: String(describing: HeadlineCell.self))
    }
    
    func setupRefreshCotnrol() {
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(handleRefreshAction), for: .valueChanged)
    }

    func setupSearchBar() {
        navigationItem.titleView = searchBar
        searchBar.delegate = self
    }
    
    @objc func handleRefreshAction() {
        presenter.fetchHeadlines(keyword: keyword, isFirstLoad: true)
    }
}

extension HeadlinesViewController: HeadlinesViewInterface {
    func showLoading() {
        DispatchQueue.main.async {
            if !self.refreshControl.isRefreshing {
                self.refreshControl.beginRefreshing()
            }
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }

    func showHeadlines(cells: [HeadlineCellModel]) {
        self.cells = cells
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showErrorView() {
        DispatchQueue.main.async {
            self.showErrorViewController(
                title: "Ada masalah saat menampilkan halaman ini",
                subTitle: "Muat ulang atau coba lagi nanti",
                buttonTitle: "Muat ulang",
                retry: {
                    self.hideErrorViewController()
                    self.presenter.fetchHeadlines(
                        keyword: self.keyword,
                        isFirstLoad: true
                    )
                }
            )
        }
    }
    
    func showMessage(_ message: String) {
        DispatchQueue.main.async {
            self.showToast(with: message)
        }
    }
}

extension HeadlinesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cells.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: HeadlineCell.self),
            for: indexPath
        ) as? HeadlineCell else { fatalError() }
        
        let item = cells[indexPath.row]
        cell.configure(item)

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.navigateToWebPage(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        presenter.headleLoadMore(row: indexPath.row)
    }
}

extension HeadlinesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.keyword = searchText
        presenter.fetchHeadlines(keyword: keyword, isFirstLoad: true)
    }
}
