import Foundation
import NewsCore

public protocol SourcesInteractorInterface {
    func fetchSources(keyword: String?, completion: @escaping (Result<[Source]?, Error>) -> Void)
}

final public class SourcesInteractor: SourcesInteractorInterface {
    
    let service: NewsServicesInterfaces
    let category: String
    
    init(category: String, service: NewsServicesInterfaces) {
        self.service = service
        self.category = category
    }
    
    public func fetchSources(keyword: String?, completion: @escaping (Result<[Source]?, Error>) -> Void) {        
        service.fetchSources(category: category) { result in
            switch result {
            case .success(let response):
                if let keyword, !keyword.isEmpty {
                    let filteredSources = response.sources?.filter { source in
                        let source = source.name ?? ""
                        return source.contains(keyword)
                    }
                    completion(.success(filteredSources))
                } else {
                    completion(.success(response.sources))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
