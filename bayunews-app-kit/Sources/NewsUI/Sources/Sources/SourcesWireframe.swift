import Foundation
import BNUIKit
import NewsCore
import UIKit

protocol SourcesWireframeInterface {
    func navigateToHeadlinesPage(source: String)
}

public protocol SourcesViewControllerFactory {
    func makeHeadlinesViewController(category: String, source: String) -> UIViewController
}

public class SourcesWireframe: BaseWireframe {
    let category: String
    let factory: SourcesViewControllerFactory
    
    public init(factory: SourcesViewControllerFactory, category: String) {
        self.factory = factory
        self.category = category
        let moduleViewController = SourcesViewController()
        super.init(viewController: moduleViewController)

        let interactor = SourcesInteractor(
            category: category,
            service: NewsService()
        )
        
        let formatter = SourcesFormatter()
        
        let presenter = SourcesPresenter(
            view: moduleViewController,
            interactor: interactor,
            wireframe: self,
            formatter: formatter
        )
        moduleViewController.presenter = presenter
    }
}

extension SourcesWireframe: SourcesWireframeInterface {
    func navigateToHeadlinesPage(source: String) {
        let destination = factory.makeHeadlinesViewController(category: category, source: source)
        navigationController?.pushViewController(destination, animated: true)
    }
}
