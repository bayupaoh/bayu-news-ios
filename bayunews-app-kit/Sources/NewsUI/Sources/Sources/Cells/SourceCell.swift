import UIKit
import BNUIKit

struct SourceCellModel {
    let title: String?
    let desc: String?
}

final class SourceCell: UITableViewCell {
    private let titleLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .black
        view.textAlignment = .left
        view.font = .boldSystemFont(ofSize: 20.0)
        view.numberOfLines = 0
        return view
    }()
    
    private let descLabel: UILabel = {
        let view = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .black
        view.textAlignment = .left
        view.font = .systemFont(ofSize: 12.0)
        view.numberOfLines = 0
        return view
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        setupView()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("Unimplemented")
    }
    
    func configure(_ cellModel: SourceCellModel) {
        titleLabel.text = cellModel.title
        descLabel.text = cellModel.desc
    }
}

private extension SourceCell {
    func setupView() {
        contentView.addSubview(titleLabel)
        titleLabel.anchorLeadingTrailing(
            toLeading: contentView.leadingAnchor,
            toTrailing: contentView.trailingAnchor,
            constant: .init(16.0, 16.0)
        )
        titleLabel.anchorTop(to: contentView.topAnchor, constant: 4.0)
        
        contentView.addSubview(descLabel)
        descLabel.anchorLeadingTrailing(
            toLeading: contentView.leadingAnchor,
            toTrailing: contentView.trailingAnchor,
            constant: .init(16.0, 16.0)
        )
        descLabel.anchorTop(to: titleLabel.bottomAnchor, constant: 4.0)
        descLabel.anchorBottom(to: contentView.bottomAnchor, constant: 4.0)
    }
}
