import Foundation
import NewsCore

protocol SourcesFormatterInterface {
    func makeCampaignCellModels(_ sources: [Source]) -> [SourceCellModel]
}

public class SourcesFormatter: SourcesFormatterInterface {
    init() { }
    
    func makeCampaignCellModels(_ sources: [Source]) -> [SourceCellModel] {
        let cells = sources.map { SourceCellModel(title: $0.name, desc: $0.description) }
        return cells
    }
}
