import Foundation
import NewsCore

protocol SourcesPresenterInterface {
    func fetchHeadlines(keyword: String?)
    func navigateToHeadlinesPage(row: Int)
}

final class SourcesPresenter: SourcesPresenterInterface {
    private unowned let view: SourcesViewInterface
    private let interactor: SourcesInteractorInterface
    private let wireframe: SourcesWireframeInterface
    private let formatter: SourcesFormatterInterface
    
    private var sources: [Source] = []
    
    init(
        view: SourcesViewInterface,
        interactor: SourcesInteractorInterface,
        wireframe: SourcesWireframeInterface,
        formatter: SourcesFormatterInterface
    ) {
        self.view = view
        self.interactor = interactor
        self.wireframe = wireframe
        self.formatter = formatter
    }

    func fetchHeadlines(keyword: String?) {
        guard (keyword ?? "").count >= 3 || (keyword ?? "").isEmpty else {
            view.showToast(message: "Minimum 3 characters for keywords")
            return
        }
        
        view.showLoading()
        interactor.fetchSources(keyword: keyword) { [weak self] result in
            guard let self else { return }
            self.view.hideLoading()
            switch result {
            case .success(let sources):
                self.sources = sources ?? []
                let cells = self.formatter.makeCampaignCellModels(sources ?? [])
                self.view.showSources(sources: cells)
            case .failure:
                self.view.showError()
            }
        }
    }
    
    func navigateToHeadlinesPage(row: Int) {
        guard let source = sources[row].id else { return }
        wireframe.navigateToHeadlinesPage(source: source)
    }
}
