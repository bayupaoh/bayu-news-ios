import UIKit
import BNUIKit

protocol SourcesViewInterface: AnyObject {
    func showLoading()
    func hideLoading()
    func showSources(sources: [SourceCellModel])
    func showError()
    func showToast(message: String)
}

final class SourcesViewController: UIViewController, ErrorableViewController {

    private let tableView: UITableView = {
        let view = UITableView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.rowHeight = UITableView.automaticDimension
        view.estimatedRowHeight = 40.0
        view.separatorStyle = .singleLine
        view.keyboardDismissMode = .onDrag
        return view
    }()

    private let refreshControl: UIRefreshControl = {
        let view = UIRefreshControl()
        view.backgroundColor = .white
        view.tintColor = .systemBlue
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let searchBar: UISearchBar = {
        let view = UISearchBar()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.placeholder = "Find your source"
        view.tintColor = .black
        return view
    }()

    var errorViewController: ErrorViewController?
    var presenter: SourcesPresenterInterface!

    private var sources: [SourceCellModel] = []
    private var keyword: String?

    init() {
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setupTableView()
        setupRefreshControl()
        setupSearchBar()
        presenter.fetchHeadlines(keyword: keyword)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        searchBar.resignFirstResponder()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        searchBar.resignFirstResponder()
    }
}

private extension SourcesViewController {
    func setupTableView() {
        view.addSubview(tableView)
        tableView.anchorAll(to: view)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(SourceCell.self, forCellReuseIdentifier: String(describing: SourceCell.self))
    }
    
    func setupRefreshControl() {
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(handleRefreshAction), for: .valueChanged)
    }

    func setupSearchBar() {
        navigationItem.titleView = searchBar
        searchBar.delegate = self
    }
    
    @objc func handleRefreshAction() {
        presenter.fetchHeadlines(keyword: keyword)
    }
}

extension SourcesViewController: SourcesViewInterface {
    func showLoading() {
        DispatchQueue.main.async {
            if !self.refreshControl.isRefreshing {
                self.refreshControl.beginRefreshing()
            }
        }
    }
    
    func hideLoading() {
        DispatchQueue.main.async {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    func showSources(sources: [SourceCellModel]) {
        self.sources = sources
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func showError() {
        DispatchQueue.main.async {
            self.showErrorViewController(
                title: "Ada masalah saat menampilkan halaman ini",
                subTitle: "Muat ulang atau coba lagi nanti",
                buttonTitle: "Muat ulang",
                retry: {
                    self.hideErrorViewController()
                    self.presenter.fetchHeadlines(keyword: self.keyword)
                }
            )
        }
    }
    
    func showToast(message: String) {
        DispatchQueue.main.async {
            self.showToast(with: message)
        }
    }
}

extension SourcesViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sources.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(
            withIdentifier: String(describing: SourceCell.self),
            for: indexPath
        ) as? SourceCell else { fatalError() }
        
        let item = sources[indexPath.row]
        cell.configure(item)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.navigateToHeadlinesPage(row: indexPath.row)
    }
}


extension SourcesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.keyword = searchText
        presenter.fetchHeadlines(keyword: self.keyword)
    }
}
