import UIKit

open class BaseWireframe {

    fileprivate unowned var _viewController: UIViewController

    // To retain view controller reference upon first access
    fileprivate var _temporaryStoredViewController: UIViewController?

    public init(viewController: UIViewController) {
        _temporaryStoredViewController = viewController
        _viewController = viewController
    }
}

public extension BaseWireframe {

    var viewController: UIViewController {
        defer { _temporaryStoredViewController = nil }
        return _viewController
    }

    var navigationController: UINavigationController? {
        return viewController.navigationController
    }
}
