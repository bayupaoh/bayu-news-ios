import UIKit
import Kingfisher

public extension UIImageView {
    func loadImage(url: String?) {
        guard let urlString = url,
              let url = URL(string: url ?? "") else {
            self.backgroundColor = .lightGray
            return
        }
        self.kf.setImage(with: url)
    }
}
