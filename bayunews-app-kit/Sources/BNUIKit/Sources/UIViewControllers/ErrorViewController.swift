import UIKit

public final class ErrorViewController: UIViewController {
    
    private let stackView: UIStackView = {
        let stackView: UIStackView = UIStackView()
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 16
        return stackView
    }()
    
    private let imageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.contentMode = .scaleAspectFit
        return view
    }()
    
    private let titleLabel: UILabel = {
        let view: UILabel = UILabel()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.text = "Ada masalah saat menampilkan halaman beranda"
        view.textAlignment = .center
        view.textColor = .black
        view.font = .boldSystemFont(ofSize: 16.0)
        view.numberOfLines = 2
        view.isHidden = true
        return view
    }()
    
    private let subtitleLabel: UILabel = {
        let view = UILabel()
        view.text = "Maaf, terjadi kesalahan. Cobalah beberapa saat lagi."
        view.textColor = .gray
        view.font = .systemFont(ofSize: 12.0)
        view.textAlignment = .center
        view.numberOfLines = 0
        view.textAlignment = .center
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let retryButton: UIButton = {
        let view = UIButton()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("Coba Lagi", for: .normal)
        view.setTitleColor(.white, for: .normal)
        view.backgroundColor = .systemBlue
        return view
    }()
    
    public let retry: () -> Void
    
    init(
        image: UIImage?,
        retry: @escaping () -> Void,
        title: String?,
        subtitle: String? = nil,
        buttonTitle: String? = nil
    ) {
        
        self.imageView.image = image
        self.retry = retry
        if let title {
            titleLabel.isHidden = false
            titleLabel.text = title
        }
        
        if let subtitle {
            subtitleLabel.text = subtitle
        }
        
        if let buttonTitle {
            retryButton.setTitle(buttonTitle, for: .normal)
        }
        
        super.init(nibName: nil, bundle: nil)
        setupViews()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension ErrorViewController {
    
    func setupViews() {
        
        view.backgroundColor = .white
        retryButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        view.addSubview(stackView)
        retryButton.anchorHeightSize(constant: 48, type: .constant)
        stackView.addArrangedSubview(imageView)
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(subtitleLabel)
        stackView.addArrangedSubview(retryButton)
        stackView.anchorCenterXY()
        stackView.anchorLeading(to: view.leadingAnchor, constant: 0)
        stackView.anchorTrailing(to: view.trailingAnchor, constant: 0)
        
        NSLayoutConstraint.activate([
            imageView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor, constant: 74),
            imageView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: -74),
            imageView.heightAnchor.constraint(equalToConstant: 130),

            titleLabel.leadingAnchor.constraint(equalTo: stackView.leadingAnchor, constant: 45),
            titleLabel.trailingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: -45),
            subtitleLabel.leadingAnchor.constraint(equalTo: stackView.leadingAnchor, constant: 52),
            subtitleLabel.trailingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: -52),

            retryButton.leadingAnchor.constraint(equalTo: stackView.leadingAnchor, constant: 32),
            retryButton.trailingAnchor.constraint(equalTo: stackView.trailingAnchor, constant: -32),
            retryButton.heightAnchor.constraint(equalToConstant: 48)
        ])
    }
    
    @objc func buttonAction() {
        retry()
    }
}
