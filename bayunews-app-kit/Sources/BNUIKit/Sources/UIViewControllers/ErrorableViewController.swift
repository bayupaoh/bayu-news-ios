import UIKit

public protocol ErrorableViewController where Self: UIViewController {
    
    var errorViewController: ErrorViewController? { get set }
    
    func showErrorViewController(
        includeNavigationBar: Bool,
        image: UIImage?,
        title: String?,
        subTitle: String?,
        buttonTitle: String?,
        allowUserDismiss: Bool,
        retry: @escaping () -> Void
    )
    func hideErrorViewController()
}

extension ErrorableViewController where Self: UIViewController {
    
    public func showErrorViewController(
        includeNavigationBar: Bool = false,
        image: UIImage? = nil,
        title: String? = nil,
        subTitle: String? = nil,
        buttonTitle: String? = nil,
        allowUserDismiss: Bool = false,
        retry: @escaping () -> Void
    ) {
        guard self.errorViewController == nil else { return }
        
        let errorViewController = ErrorViewController(
            image: image,
            retry: retry,
            title: title,
            subtitle: subTitle,
            buttonTitle: buttonTitle
        )

        if includeNavigationBar,
           let navigationController = self.navigationController {
            navigationController.add(errorViewController)
        } else {
            self.add(errorViewController)
        }
        
        NSLayoutConstraint.activate([
            errorViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            errorViewController.view.leadingAnchor.constraint(equalTo: view.trailingAnchor),
            errorViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            errorViewController.view.topAnchor.constraint(equalTo: view.topAnchor)
        ])
        
        self.errorViewController = errorViewController
        
        if !allowUserDismiss {
            self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        }
    }
    
    public func hideErrorViewController() {
        self.errorViewController?.remove()
        self.errorViewController = nil
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
}

public extension UIViewController {
    
    func add(
        _ child: UIViewController,
        frame: CGRect? = nil,
        to targetView: UIView? = nil,
        animated: Bool = false,
        anchorToSuperview: Bool = true
    ) {
        addChild(child)
        
        child.view.alpha = animated ? 0.0 : 1.0
        
        if let frame = frame {
            child.view.frame = frame
        }
        
        if let targetView = targetView {
            targetView.addSubview(child.view)
            
            if anchorToSuperview {
                child.view.anchorAll(to: targetView)
            }

        } else {
            view.addSubview(child.view)

            if anchorToSuperview {
                child.view.anchorAll(to: view)
            }
        }
        
        if animated {
            UIView.animate(withDuration: 0.33, animations: {
                child.view.alpha = 1.0
            })
        }
        
        child.didMove(toParent: self)
    }
    
    func remove(animated: Bool = false) {
        willMove(toParent: nil)
        if animated {
            UIView.animate(withDuration: 0.2, animations: {
                self.view.alpha = 0
            }, completion: { _ in
                self.view.removeFromSuperview()
            })
        } else {
            self.view.removeFromSuperview()
        }
        removeFromParent()
    }
}
