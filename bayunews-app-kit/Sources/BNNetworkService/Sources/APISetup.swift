import Foundation

public var isLoggingModeEnabled = true//ProcessInfo.processInfo.environment["LOGGING_MODE"] == "TRUE"

public enum HTTPMethod: String {
    case GET
    case POST
}

public typealias HTTPHeaders = [String : String]
public typealias HTTPParameters = [String : Any]

public protocol APISetup {
    var baseURL: URL { get }
    var endpoint: String { get }
    var method: HTTPMethod { get }
    var parameters: HTTPParameters { get }
    var authorization: String? { get }
}

extension APISetup {
    func makeURLRequest() -> URLRequest? {
        if let url = URL(string: baseURL.absoluteString + endpoint) {
            var request: URLRequest = url.setParameter(parameters: parameters, method: method)
            request.httpMethod = method.rawValue
            if isLoggingModeEnabled { logRequest(withRequest: request) }
            return request
        }
        return nil
    }
    
    func logRequest(withRequest request: URLRequest) {
        print("======================START REQUESTING===============================")
        print("HEADER = \(String(describing: request.allHTTPHeaderFields))")
        print("BODY = \(String(data: request.httpBody ?? Data(), encoding: .ascii) ?? "EMPTY")")
        print("URL = \(String(describing: request.url?.absoluteString))")
        print("METHOD = \(request.httpMethod ?? "NONE")")
        print("======================FINISH REQUESTING===============================")
    }
}
