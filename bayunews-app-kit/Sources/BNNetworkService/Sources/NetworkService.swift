import Foundation

// https://betterprogramming.pub/network-requests-in-swift-using-result-type-and-generics-cfbcea464c1f

public enum RequestError: Error {
    case clientError
    case serverError
    case dataDecodingError
    case noData
    case urlInvalid
}

public protocol NetworkServiceInterface {
    func makeUrlRequest<T: Decodable>(config: APISetup, completion: @escaping (Result<T, RequestError>) -> Void)
}

final public class NetworkService: NetworkServiceInterface {
    public init() {}
    
    public func makeUrlRequest<T: Decodable>(config: APISetup, completion: @escaping (Result<T, RequestError>) -> Void) {
        
        guard let request = config.makeURLRequest() else {
            completion(.failure(.urlInvalid))
            return
        }
        let urlTask = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil else {
                completion(.failure(.clientError))
                return
            }
            
            guard let response = response as? HTTPURLResponse, 200...299 ~= response.statusCode else {
                completion(.failure(.serverError))
                return
            }
            
            guard let data = data else {
                completion(.failure(.noData))
                return
            }
            
            if isLoggingModeEnabled {
                print("======================START RESPONDING===============================")
                print("BODY RESPONSE = \(String(data: data, encoding: .ascii) ?? "Empty Response")")
                print("======================FINISH RESPONDING===============================")
            }
            
            guard let decodedData: T = self.decodedData(data) else {
                completion(.failure(.dataDecodingError))
                return
            }
            
            completion(.success(decodedData))
        }
        
        urlTask.resume()
    }
}

private extension NetworkService {
    func decodedData<T: Decodable>(_ data: Data) -> T? {
        return try? JSONDecoder().decode(T.self, from: data)
    }
}
