import Foundation

public struct CategoryItem {
    public let title: String?
    public let value: String?
    public init(title: String?, value: String?) {
        self.title = title
        self.value = value
    }
}
