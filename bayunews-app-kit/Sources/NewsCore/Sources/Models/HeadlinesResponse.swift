import Foundation

// MARK: - HeadlinesResponse
public struct HeadlinesResponse: Codable {
    public let status: String?
    public let totalResults: Int?
    public let articles: [Article]?
}

// MARK: - Article
public struct Article: Codable {
    public let source: Source?
    public let author, title, description: String?
    public let url: String?
    public let urlToImage: String?
    public let publishedAt: String?
    public let content: String?
}

extension HeadlinesResponse {
    public static var mock: HeadlinesResponse = HeadlinesResponse(
        status: "ok",
        totalResults: 1002,
        articles: [
            Article(
                source: nil,
                author: "Judith Graham",
                title: "What seniors should know before trying Ozempic, new weight-loss drugs - The Washington Post",
                description: """
Obesity affects more than 4 in 10 people 60 and older, but experts advise caution for seniors considering new weight-loss medications.
""",
                url: "https://www.washingtonpost.com/wellness/2023/07/18/aging-weight-loss-semaglutide/",
                urlToImage: "https://www.washingtonpost.com/wp-apps/imrs.php?src=https://arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/HU4UV63PQSF7KS6OQILH4IF6KA.jpg&w=1440",
                publishedAt: "2023-07-18T18:24:31Z",
                content: "Comment on this story\r\nComment\r\nCorlee Morris has dieted"
            )
        ])
}
