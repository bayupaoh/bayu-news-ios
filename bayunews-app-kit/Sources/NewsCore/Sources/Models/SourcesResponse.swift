import Foundation

// MARK: - SourcesResponse
public struct SourcesResponse: Codable {
    public let status: String?
    public let sources: [Source]?
    
    enum CodingKeys: String, CodingKey {
        case status, sources
    }
}

// MARK: - Source
public struct Source: Codable {
    public let id, name, description: String?
    public let url: String?
    public let category, language, country: String?
}

extension SourcesResponse {
    public static var mock: SourcesResponse = SourcesResponse(
        status: "ok",
        sources: [.mock]
    )
}

extension Source {
    public static var mock: Source = Source(
        id: "australian-financial-review",
        name: "Australian Financial Review",
        description: """
The Australian Financial Review reports the latest news from business, finance, investment and politics, updated in real time. It has a reputation for independent, award-winning journalism and is essential reading for the business and investor community.
""",
        url: "http://www.afr.com",
        category: "business",
        language: "en",
        country: "au"
    )
}
