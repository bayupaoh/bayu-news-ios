import Foundation
import BNNetworkService

public protocol NewsServicesInterfaces {
    func fetchCategories() -> [CategoryItem]
    func fetchSources(
        category: String,
        completion: @escaping (Result<SourcesResponse, Error>) -> Void
    )
    func fetchHeadlines(
        source: String,
        category: String,
        keyword: String?,
        page: Int?,
        completion: @escaping (Result<HeadlinesResponse, Error>) -> Void
    )
}

public final class NewsService: NewsServicesInterfaces {
    private let service: NetworkServiceInterface
    
    public init(service: NetworkServiceInterface = NetworkService()) {
        self.service = service
    }
    
    public func fetchCategories() -> [CategoryItem] {
        return [
            CategoryItem(title: "Business", value: "business"),
            CategoryItem(title: "Entertainment", value: "entertainment"),
            CategoryItem(title: "General", value: "general"),
            CategoryItem(title: "Health", value: "health"),
            CategoryItem(title: "Science", value: "science"),
            CategoryItem(title: "Sports", value: "sports"),
            CategoryItem(title: "Technology", value: "technology")
        ]
    }
    
    // API doesnt support pagination
    public func fetchSources(
        category: String,
        completion: @escaping (Result<SourcesResponse, Error>) -> Void
    ) {
        let params: HTTPParameters = [
            "category": category,
            "language": "en"
        ]
        let config = NewsEndPoint.sources(params)
        service.makeUrlRequest(config: config) { (result: Result<SourcesResponse, RequestError>) in
            switch result {
            case .success(let success):
                completion(.success(success))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func fetchHeadlines(
        source: String,
        category: String,
        keyword: String?,
        page: Int?,
        completion: @escaping (Result<HeadlinesResponse, Error>) -> Void
    ) {
        var params: HTTPParameters = [
            "q": keyword ?? "",
            "category": category,
            "source": source        ]
        if let page { params["page"] = page }
        let config = NewsEndPoint.headlines(params)
        service.makeUrlRequest(config: config) { (result: Result<HeadlinesResponse, RequestError>) in
            switch result {
            case .success(let success):
                completion(.success(success))
            case .failure(let failure):
                completion(.failure(failure))
            }
        }
    }
}
