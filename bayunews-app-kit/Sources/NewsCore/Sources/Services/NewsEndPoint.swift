import Foundation
import BNNetworkService

public enum NewsEndPoint {
    case sources(HTTPParameters)
    case headlines(HTTPParameters)
}

extension NewsEndPoint: APISetup {
    public var baseURL: URL {
        return URL(string: "https://newsapi.org")!
    }
    
    public var endpoint: String {
        switch self {
        case .headlines: return "/v2/top-headlines"
        case .sources: return "/v2/top-headlines/sources"
        }
    }
    
    public var method: HTTPMethod {
        return .GET
    }
    
    public var parameters: HTTPParameters {
        switch self {
        case let .sources(parameters),
            let .headlines(parameters):
            let apiKey = ProcessInfo.processInfo.environment["API_KEY"]
            var params = parameters
            params["apiKey"] = apiKey
            return params
        }
    }
    
    public var authorization: String? {
        return ProcessInfo.processInfo.environment["API_KEY"]
    }
    
}
