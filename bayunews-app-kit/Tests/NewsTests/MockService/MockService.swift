import Foundation
import NewsCore
import BNNetworkService

final class MockService: NewsServicesInterfaces {
    fileprivate var simulateSuccess: Bool = true

    init(simulateSuccess: Bool) {
        self.simulateSuccess = simulateSuccess
    }
    
    func fetchCategories() -> [NewsCore.CategoryItem] {
        return [
            CategoryItem(title: "Business", value: "business"),
            CategoryItem(title: "Entertainment", value: "entertainment"),
            CategoryItem(title: "General", value: "general")
        ]
    }
    
    func fetchSources(category: String, completion: @escaping (Result<NewsCore.SourcesResponse, Error>) -> Void) {
        if simulateSuccess {
            completion(.success(.mock))
        } else {
            completion(.failure(RequestError.clientError))
        }
    }
    
    func fetchHeadlines(source: String, category: String, keyword: String?, page: Int?, completion: @escaping (Result<NewsCore.HeadlinesResponse, Error>) -> Void) {
        if simulateSuccess {
            completion(.success(.mock))
        } else {
            completion(.failure(RequestError.clientError))
        }

    }
    
    
}
