import XCTest
@testable import NewsUI
final class SourcesInteractorTests: XCTestCase {

    func testFetchSourcesSuccess() throws {
        let service = MockService(simulateSuccess: true)
        let sut = SourcesInteractor(category: "business", service: service)
        sut.fetchSources(keyword: nil) { result in
            switch result {
            case .success(let response):
                XCTAssertEqual(response?.count, 1)
            case .failure(let error):
                XCTFail("An error occured: \(error)")
            }
        }
    }

    func testFetchSourcesSuccessWithKeyword() throws {
        let service = MockService(simulateSuccess: true)
        let sut = SourcesInteractor(category: "business", service: service)
        sut.fetchSources(keyword: "Australian") { result in
            switch result {
            case .success(let response):
                XCTAssertEqual(response?.count, 1)
            case .failure(let error):
                XCTFail("An error occured: \(error)")
            }
        }
    }
    
    func testFetchSourcesFailed() throws {
        let service = MockService(simulateSuccess: false)
        let sut = SourcesInteractor(category: "business", service: service)
        sut.fetchSources(keyword: nil) { result in
            switch result {
            case .success:
                XCTFail("It shouldnt error happened")
            case .failure(let error):
                XCTAssertNotNil(error)
            }
        }
    }
}
