import XCTest
import NewsCore
import BNNetworkService
@testable import NewsUI

final class SourcesPresenterTests: XCTestCase {
    private let mockView = MockSourcesView()
    private let mockWireframe = MockSourcesWireframe()
    
    func testFetchHeadlinesSuccess() throws {
        let mockInteractor = MockSourcesInteractor(isSimulateSucces: true)
        let sut = SourcesPresenter(
            view: mockView,
            interactor: mockInteractor,
            wireframe: mockWireframe,
            formatter: SourcesFormatter()
        )
        sut.fetchHeadlines(keyword: nil)
        XCTAssertEqual(mockView.cells.count, 1)
    }
    
    func testFetchHeadlinesKeywordInvalid() throws {
        let mockInteractor = MockSourcesInteractor(isSimulateSucces: true)
        let sut = SourcesPresenter(
            view: mockView,
            interactor: mockInteractor,
            wireframe: mockWireframe,
            formatter: SourcesFormatter()
        )
        sut.fetchHeadlines(keyword: "a")
        XCTAssertEqual(mockView.message, "Minimum 3 characters for keywords")
    }

    func testFetchHeadlinesFailed() throws {
        let mockInteractor = MockSourcesInteractor(isSimulateSucces: false)
        let sut = SourcesPresenter(
            view: mockView,
            interactor: mockInteractor,
            wireframe: mockWireframe,
            formatter: SourcesFormatter()
        )
        sut.fetchHeadlines(keyword: nil)
        XCTAssertTrue(mockView.isShowError)
    }

    func testNavigateToHeadlinesPage() throws {
        let mockInteractor = MockSourcesInteractor(isSimulateSucces: true)
        let sut = SourcesPresenter(
            view: mockView,
            interactor: mockInteractor,
            wireframe: mockWireframe,
            formatter: SourcesFormatter()
        )
        sut.fetchHeadlines(keyword: nil)
        sut.navigateToHeadlinesPage(row: 0)
        XCTAssertTrue(mockWireframe.isNavigatingToHeadlinesPage)
    }
}

final class MockSourcesView: SourcesViewInterface {
    var isLoading: Bool = false
    var cells: [SourceCellModel] = []
    var isShowError: Bool = false
    var message: String?
    
    func showLoading() {
        isLoading = true
    }
    
    func hideLoading() {
        isLoading = false
    }
    
    func showSources(sources: [SourceCellModel]) {
        cells = sources
    }
    
    func showError() {
        isShowError = true
    }
    
    func showToast(message: String) {
        self.message = message
    }
}

final class MockSourcesInteractor: SourcesInteractorInterface {
    let isSimulateSucces: Bool
    init(isSimulateSucces: Bool) {
        self.isSimulateSucces = isSimulateSucces
    }
    func fetchSources(keyword: String?, completion: @escaping (Result<[NewsCore.Source]?, Error>) -> Void) {
        if isSimulateSucces {
            completion(.success([.mock]))
        } else {
            completion(.failure(RequestError.clientError))
        }
    }
}

final class MockSourcesWireframe: SourcesWireframeInterface {
    var isNavigatingToHeadlinesPage: Bool = false
    func navigateToHeadlinesPage(source: String) {
        isNavigatingToHeadlinesPage = true
    }
}
