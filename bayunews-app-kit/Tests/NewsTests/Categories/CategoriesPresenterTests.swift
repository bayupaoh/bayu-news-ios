import XCTest
import NewsCore
@testable import NewsUI

final class CategoriesPresenterTests: XCTestCase {
    let view = MockCategoriesView()
    let interactor = MockCategoriesInteractor()
    let wireframe = MockCategoriesWireframe()

    func testViewDidLoad() throws {
        let sut = CategoriesPresenter(
            view: view,
            interactor: interactor,
            wireframe: wireframe
        )
        sut.viewDidLoad()
        XCTAssertEqual(view.categories.count, 3)
    }
    
    func testNavigateToSourcePage() throws {
        let sut = CategoriesPresenter(
            view: view,
            interactor: interactor,
            wireframe: wireframe
        )
        sut.navigateToSourcePage(category: "id-source")
        XCTAssertTrue(wireframe.isNavigateToSourcesPage)
    }

}

final class MockCategoriesView: CategoriesViewInterface {
    var categories: [CategoryItem] = []
    func setCategories(categories: [CategoryItem]) {
        self.categories = categories
    }
}

final class MockCategoriesInteractor: CategoriesInteractorInterface {
    func fetchCategories() -> [CategoryItem] {
        return [
            CategoryItem(title: "Business", value: "business"),
            CategoryItem(title: "Entertainment", value: "entertainment"),
            CategoryItem(title: "General", value: "general")
        ]
    }
    
    
}

final class MockCategoriesWireframe: CategoriesWireframeInterface {
    var isNavigateToSourcesPage: Bool = false
    func navigateToSourcesPage(category: String) {
        self.isNavigateToSourcesPage = true
    }
}
