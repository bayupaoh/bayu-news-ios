import XCTest
@testable import NewsUI

final class CategoriesInteractorTests: XCTestCase {
    func testFetchCategories() throws {
        let mockService = MockService(simulateSuccess: true)
        let sut = CategoriesInteractor(service: mockService)
        let categories = sut.fetchCategories()
        XCTAssertEqual(categories.count, 3)
    }
}
