// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let news = "News"
let networkService = "BNNetworkService"
let bnUIKit = "BNUIKit"
let common = "Common"

private extension String {
    var tests: String {
        "\(self)Tests"
    }
    
    var ui: String {
        "\(self)UI"
    }

    var core: String {
        "\(self)Core"
    }
    
    var library: Product {
        .library(name: self, targets: [self])
    }

    var dependency : Target.Dependency {
        .init(stringLiteral: self)
    }
}

private extension Target.Dependency {
    static let kingFisher = Self.product(name: "Kingfisher", package: "Kingfisher")
}

let package = Package(
    name: "bayunews-app-kit",
    platforms: [.iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        networkService.library,
        news.core.library,
        news.ui.library,
        bnUIKit.library,
        common.ui.library
    ],
    dependencies: [
        .package(url: "https://github.com/onevcat/Kingfisher.git", exact: "7.8.1"),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: bnUIKit,
            dependencies: [
                .kingFisher
            ]
        ),
        .target(
            name: common.ui,
            dependencies: [
                bnUIKit.dependency
            ]
        ),
        .target(
            name: networkService,
            dependencies: []
        ),
        .target(
            name: news.core,
            dependencies: [
                networkService.dependency
            ]
        ),
        .target(
            name: news.ui,
            dependencies: [
                news.core.dependency,
                bnUIKit.dependency
            ]
        ),
        .testTarget(
            name: news.tests,
            dependencies: [
                news.ui.dependency,
                news.core.dependency,
                networkService.dependency
            ]
        )
    ]
)
